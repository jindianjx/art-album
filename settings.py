OUTPUT_IMAGE = 'output/output'

VGG_MODEL_PATH = 'imagenet-vgg-verydeep-19.mat'

IMAGE_WIDTH = 450

IMAGE_HEIGHT = 300

CONTENT_LOSS_LAYERS = [('conv4_2', 0.5),('conv5_2',0.5)]

STYLE_LOSS_LAYERS = [('conv1_1', 0.5), ('conv2_1', 0.5)]

NOISE = 0.5

IMAGE_MEAN_VALUE = [128.0, 128.0, 128.0]

ALPHA = 1

BETA = 500

TRAIN_STEPS = 3000
