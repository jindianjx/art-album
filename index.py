from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import sys
import os
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
from PyQt5.uic import loadUiType
import datetime
from xlrd import *
import copy
import numpy
ui,_ = loadUiType('library.ui')

login,_ = loadUiType('login.ui')

pwd='123456'
dbname='library'
run_network= False

save_path='./sys/user_styles/'
path_l='./sys/all_styles/'

cur_page=1


class Login(QWidget, login):
    def __init__(self):
        QWidget.__init__(self)

        self.setupUi(self)
        self.pushButton.clicked.connect(self.Handel_Login)
        style = open('themes/darkorange.css', 'r')
        style = style.read()
        self.setStyleSheet(style)

    def Handel_Login(self):
        self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
        self.cur = self.db.cursor()
        global global_username
        global global_password
        global_username = self.lineEdit.text()
        global_password = self.lineEdit_2.text()

        sql = ''' SELECT * FROM users'''

        self.cur.execute(sql)
        data = self.cur.fetchall()
        for row in data:
            if global_username == row[1] and global_password == row[3]:
                print('user match')

                self.window2 = MainApp()
                self.close()
                self.window2.show()

            else:
                self.label.setText('Make Sure You Enterd Your Username And Password Correctly')
        

    def get_username(self):
        return self.username



class MainApp(QMainWindow, ui):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setupUi(self)
        self.Handel_UI_Changes()
        self.Handel_Buttons()
        self.Dark_Orange_Theme()
        self.Show_favs()
        self.label_arr = []
        self.cur_pg=1        
 

        self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
        self.cur = self.db.cursor()
        sql = ''' SELECT * FROM styles'''
        self.cur.execute(sql)
        self.time = numpy.array(self.cur.fetchall())
        self.length=len(self.time)-1

        if (self.length+1)%8==0:
            self.total_page =int(self.length/8)
        else:
            self.total_page=int(self.length/8)+1

        self.styles=copy.deepcopy(self.time)
        self.start_flash()
    
        
    def start_flash(self):
        label1 = QLabel(self)
        label1.setText("   显示图片")
        label1.setFixedSize(320, 240)
        label1.move(200, 350)

        label1.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName = './examples/1.jpg'
        jpg = QtGui.QPixmap(imgName).scaled(label1.width(), label1.height())
        label1.setPixmap(jpg)
        self.label_arr.append(label1)
        label1.show()

        label2 = QLabel(self)
        label2.setText("   显示图片")
        label2.setFixedSize(200, 150)
        label2.move(200, 350)

        label2.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName = './examples/2.jpg'
        jpg = QtGui.QPixmap(imgName).scaled(label2.width(), label2.height())
        label2.setPixmap(jpg)
        self.label_arr.append(label2)
        label2.show()

        label3 = QLabel(self)
        label3.setText("   显示图片")
        label3.setFixedSize(200, 150)
        label3.move(200, 350)

        label3.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName = './examples/3.jpg'
        jpg = QtGui.QPixmap(imgName).scaled(label3.width(), label3.height())
        label3.setPixmap(jpg)
        self.label_arr.append(label3)
        label3.show()



        self.listView_Anim1 = QPropertyAnimation(label1, b"geometry") 
        self.listView_Anim1.setDuration(500)
        self.listView_Anim1.setStartValue(QRect(500, -200, 0, 0))
        self.listView_Anim1.setEndValue(QRect(500, 30, 491, 221))
        self.listView_Anim1.start()

        self.listView_Anim2 = QPropertyAnimation(label2, b"geometry") 
        self.listView_Anim2.setDuration(500)
        self.listView_Anim2.setStartValue(QRect(0, 320, 0, 0)) 
        self.listView_Anim2.setEndValue(QRect(370, 320, 491, 221)) 
        self.listView_Anim2.start() 
        
        self.listView_Anim3 = QPropertyAnimation(label3, b"geometry") 
        self.listView_Anim3.setDuration(500)
        self.listView_Anim3.setStartValue(QRect(1100, 320, 0, 0))
        self.listView_Anim3.setEndValue(QRect(720, 320, 491, 221))  
        self.listView_Anim3.start()
        

    def upload_style(self):
        label1 = QLabel(self)
        label1.setText("   显示图片")
        label1.setFixedSize(300, 200)
        label1.move(450, 200)

        label1.setStyleSheet("QLabel{background:white;}"
                             "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                             )
        imgName, imgType = QFileDialog.getOpenFileName(self, "上传图片", "", "*.jpg;;*.png;;All Files(*)")
        jpg = QtGui.QPixmap(imgName).scaled(label1.width(), label1.height())
        label1.setPixmap(jpg)
        self.label_arr.append(label1)
        label1.show()

        path = save_path + global_username + '/'
        isExists = os.path.exists(path)
        if not isExists:
            os.makedirs(path)
        filename1 = path + self.lineEdit_19.text() + '.jpg'

        imgtemp = label1.pixmap().toImage()
        imgtemp.save(filename1, 'JPG', 100)
        reply = QMessageBox.information(self,
                                        "上传图片",
                                        "图片上传成功！",
                                        QMessageBox.Yes)

    def openimage(self):

        self.label = QLabel(self)
        self.label.setText("   显示图片")
        self.label.setFixedSize(300, 200)
        self.label.move(480, 100)

        self.label.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName, imgType = QFileDialog.getOpenFileName(self, "上传图片", "", "*.jpg;;*.png;;All Files(*)")
        jpg = QtGui.QPixmap(imgName).scaled(self.label.width(), self.label.height())
        self.label.setPixmap(jpg)
        self.label_arr.append(self.label)
        
        label1 = QLabel(self)
        label1.setText("   显示图片")
        label1.setFixedSize(120, 90)
        label1.move(200, 350)

        label1.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName1 = './examples/1.jpg'
        jpg1 = QtGui.QPixmap(imgName1).scaled(label1.width(), label1.height())

        label2 = QLabel(self)
        label2.setText("   显示图片")
        label2.setFixedSize(120, 90)
        label2.move(400, 350)

        label2.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName2 = './examples/2.jpg'
        jpg2 = QtGui.QPixmap(imgName2).scaled(label2.width(), label2.height())

        label3 = QLabel(self)
        label3.setText("   显示图片")
        label3.setFixedSize(120, 90)
        label3.move(600, 350)

        label3.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName3 = './examples/3.jpg'
        jpg3 = QtGui.QPixmap(imgName3).scaled(label3.width(), label3.height())
    
        label4 = QLabel(self)
        label4.setText("   显示图片")
        label4.setFixedSize(120, 90)
        label4.move(800, 350)

        label4.setStyleSheet("QLabel{background:white;}"
                                 "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                 )
        imgName4 = './examples/4.jpg'
        jpg4 = QtGui.QPixmap(imgName4).scaled(label4.width(), label4.height())

        if run_network:
            # 调用训练函数
            imgs=[]
            self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
            self.cur = self.db.cursor()

            self.cur.execute(''' SELECT * FROM favs WHERE user_name=%s''', global_username)
            data = self.cur.fetchall()

            if data:
               counter=0
               row=self.cur_pg-1
               for row, form in enumerate(data):
                  for column, item in enumerate(form):
                      if counter<2:                          
                         counter=counter+1
                         continue
                      imgs.append(train(imgName,str(item)))
                      column += 1
                  label1.setPixmap(imgs[0])
                  label2.setPixmap(imgs[1])
                  label3.setPixmap(imgs[2])
                  label4.setPixmap(imgs[3])
                  flag1=0
                  flag2=0
                  break
        else:
            # 仅仅作为展示使用
            label1.setPixmap(jpg1)        
            label2.setPixmap(jpg2)
            label3.setPixmap(jpg3) 
            label4.setPixmap(jpg4)
            
        self.label_arr.append(label1)
        self.label_arr.append(label2)
        self.label_arr.append(label3)
        self.label_arr.append(label4)
        self.label.show()
        label1.show()
        label2.show()
        label3.show()
        label4.show()

    def display_info(self):

        self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
        self.cur = self.db.cursor()

        sql = ''' SELECT * FROM users WHERE user_name=%s'''

        self.cur.execute(sql, [(global_username)])
        data = self.cur.fetchall()
        for row in data:
            self.lineEdit_9.setPlaceholderText(row[1])
            self.lineEdit_10.setPlaceholderText(row[2])

        self.lineEdit_11.setPlaceholderText('test')
        self.lineEdit_12.setPlaceholderText('test')
        print(global_username)


    def Handel_UI_Changes(self):
        self.Hiding_Themes()
        self.tabWidget.tabBar().setVisible(False)

    def Handel_Buttons(self):

        self.pushButton_my1.clicked.connect(self.Open_Books_Tab)
        self.pushButton_my2.clicked.connect(self.openimage)
        self.pushButton_my3.clicked.connect(self.display_info)

        self.pushButton_5.clicked.connect(self.Show_Themes)
        self.pushButton_17.clicked.connect(self.Hiding_Themes)

        self.pushButton.clicked.connect(self.Open_Day_To_Day_Tab)
        self.pushButton_2.clicked.connect(self.Open_Books_Tab)
        self.pushButton_26.clicked.connect(self.Open_CLients_Tab)
        self.pushButton_3.clicked.connect(self.Open_Users_Tab)
        self.pushButton_4.clicked.connect(self.Open_Settings_Tab)

        self.pushButton_14.clicked.connect(self.upload_style)
        self.pushButton_15.clicked.connect(self.Search_favs)
        self.pushButton_13.clicked.connect(self.Add_New_User)

        self.pushButton_19.clicked.connect(self.Dark_Orange_Theme)
        self.pushButton_18.clicked.connect(self.Dark_Blue_Theme)
        self.pushButton_21.clicked.connect(self.Dark_Gray_Theme)
        self.pushButton_20.clicked.connect(self.QDark_Theme)
        self.pushButton_left.clicked.connect(self.left_page)
        self.pushButton_right.clicked.connect(self.right_page)
        self.pushButton_rank.clicked.connect(self.rank)
        self.pushButton_time.clicked.connect(self.time)

        self.btn.clicked.connect(self.score)

    def Show_Themes(self):
        self.groupBox_3.show()

    def Hiding_Themes(self):
        self.groupBox_3.hide()

    ########################################
    ######### opening tabs #################
    def Open_Day_To_Day_Tab(self):
        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)
        self.tabWidget.setCurrentIndex(0)
        self.start_flash()

    def Open_Books_Tab(self):
        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)
        self.tabWidget.setCurrentIndex(1)

    def show_styles(self):
        global cur_page

        cur=(cur_page-1)*8
        if cur+7<=self.length:
            for cur in range(cur,cur+8):
                num = cur % 8
                i = num % 4
                j = int(num / 4)

                imgName = self.styles[cur][1]
                imgScore=self.styles[cur][3]
                imgId=self.styles[cur][0]
                label = QLabel(self)
                label.setFixedSize(200, 140)
                label.move(205 + 230 * i, 100 + j * 195)
                label.setStyleSheet("QLabel{background:white;}"
                                    "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                    )
                path = path_l + imgName + '.jpg'
                jpg = QtGui.QPixmap(path).scaled(label.width(), label.height())
                label.setPixmap(jpg)

                label0 = QLabel(self)
                label0.setFixedSize(200, 35)
                label0.move(205 + 230 * i, 240 + j * 200)
                label0.setAlignment(Qt.AlignCenter)
                label0.setText(imgId+"."+imgName + "（" + str(imgScore) + "分）")

                self.label_arr.append(label)
                self.label_arr.append(label0)
                label.show()
                label0.show()
        else:
            for cur in range(cur,self.length+1):
                num = cur % 8
                i = num % 4
                j = int(num / 4)
                imgName = self.styles[cur][1]
                imgScore = self.styles[cur][3]
                imgId = self.styles[cur][0]
                label = QLabel(self)
                label.setFixedSize(200, 140)
                label.move(205 + 230 * i, 100 + j * 200)
                label.setStyleSheet("QLabel{background:white;}"
                                    "QLabel{color:rgb(300,300,300,120);font-size:10px;font-weight:bold;font-family:宋体;}"
                                    )
                path = path_l + imgName + '.jpg'
                jpg = QtGui.QPixmap(path).scaled(label.width(), label.height())
                label.setPixmap(jpg)

                label0 = QLabel(self)
                label0.setFixedSize(200, 35)
                label0.move(205 + 230 * i, 240 + j * 200)
                label0.setAlignment(Qt.AlignCenter)
                label0.setText(imgId+"."+imgName + "（" + str(imgScore) + "分）")

                self.label_arr.append(label)
                self.label_arr.append(label0)
                label.show()
                label0.show()

    def Open_CLients_Tab(self):
        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)
        self.show_styles()
        self.tabWidget.setCurrentIndex(2)

    def right_page(self):
        global cur_page

        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)

        cur_page=cur_page+1
        if cur_page>=self.total_page:
            cur_page=self.total_page
        self.show_styles()

        self.tabWidget.setCurrentIndex(2)

    def left_page(self):
        global cur_page

        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)

        cur_page=cur_page-1
        if cur_page<=0:
            cur_page=1
        self.show_styles()

        self.tabWidget.setCurrentIndex(2)

    def quick_sort(self,styles, start, end):
        if start >= end:
            return

        mid = float(styles[start][3])
        mid1=styles[start][1]
        mid2=styles[start][2]

        left = start
        right = end
        while left < right:
            while left < right and float(styles[right][3]) <= mid:
                right -= 1
            styles[left][1] = styles[right][1]
            styles[left][2] = styles[right][2]
            styles[left][3] = styles[right][3]
            while left < right and float(styles[left][3]) > mid:
                left += 1
            styles[right][1] = styles[left][1]
            styles[right][2] = styles[left][2]
            styles[right][3] = styles[left][3]

        styles[left][3] = str(mid)
        styles[left][1]=mid1
        styles[left][2]=mid2
        self.quick_sort(styles, start, left - 1)
        self.quick_sort(styles, left + 1, end)

    def rank(self):
        global  cur_page

        cur_page=1
        self.quick_sort(self.styles,0,self.length)
        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)

        self.show_styles()
        self.tabWidget.setCurrentIndex(2)

    def time(self):
        global cur_page

        cur_page = 1
        self.styles=copy.deepcopy(self.time)

        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)

        self.show_styles()
        self.tabWidget.setCurrentIndex(2)

    def score(self):
        self.items=[]
        self.items.append(str((cur_page-1) * 8+1))
        if cur_page!=self.total_page:
           for i in range(1,8):
               self.items.append(str(int(self.items[i-1])+1))
        else:
           for i in range(1,self.length-int(cur_page-1)*8+1):
               self.items.append(str(int(self.items[i-1])+1))

        QInputDialog.getItem(self, "请输入评分模板的序号", "这是提示信息\n\n请下拉选择序号:", self.items, 1, True)
        QInputDialog.getDouble(self, "请输入分数", "请输入1-5范围内数字:", 1.0, 1, 5, 1)

    def Open_Users_Tab(self):
        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)
        self.tabWidget.setCurrentIndex(3)

    def Open_Settings_Tab(self):
        for i in range(len(self.label_arr)):
            self.label_arr[i].setHidden(True)
        self.tabWidget.setCurrentIndex(4)

    

    ########################################
    ######### users #################

    def Add_New_User(self):
        self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
        self.cur = self.db.cursor()

        username = self.lineEdit_9.text()
        email = self.lineEdit_10.text()
        password = self.lineEdit_11.text()
        password2 = self.lineEdit_12.text()

        if password == password2:
            self.cur.execute(''' 
                INSERT INTO users(user_name , user_email , user_password)
                VALUES (%s , %s , %s)
            ''', (username, email, password))

            self.db.commit()
            self.statusBar().showMessage('New User Added')

        else:
            self.label_30.setText('please add a valid password twice')

    def Login(self):
        self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
        self.cur = self.db.cursor()

        username = self.lineEdit_14.text()
        password = self.lineEdit_13.text()

        sql = ''' SELECT * FROM users'''

        self.cur.execute(sql)
        data = self.cur.fetchall()
        for row in data:
            if username == row[1] and password == row[3]:
                print('user match')
                self.statusBar().showMessage('Valid Username & Password')
                self.groupBox_4.setEnabled(True)

                self.lineEdit_17.setText(row[1])
                self.lineEdit_15.setText(row[2])
                self.lineEdit_16.setText(row[3])

    def Edit_User(self):

        username = self.lineEdit_9.text()
        email = self.lineEdit_10.text()
        password = self.lineEdit_11.text()
        password2 = self.lineEdit_12.text()
        if (len(username) == 0 or len(email) == 0 or len(password) == 0 or len(password2) == 0):
            return

        original_name = self.lineEdit_9.text()

        if password == password2:
            self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
            self.cur = self.db.cursor()

            #print(username)
            #print(email)
            #print(password)

            self.cur.execute('''
                UPDATE users SET user_name=%s , user_email=%s , user_password=%s WHERE user_name=%s
            ''', (username, email, password, original_name))

            self.db.commit()
            self.statusBar().showMessage('User Data Updated Successfully')

        else:
            print('make sure you entered you password correctly')

    

    def Search_favs(self):

        self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
        self.cur = self.db.cursor()

        self.cur.execute(''' SELECT * FROM favs WHERE (fav1=%s or fav2=%s or fav3=%s or fav4=%s) and user_name=%s''', (
        self.lineEdit_20.text(), self.lineEdit_20.text(), self.lineEdit_20.text(), self.lineEdit_20.text(),
        global_username))
        data = self.cur.fetchall()
        self.tableWidget_3.clearContents()
        if data:
            self.tableWidget_3.setRowCount(0)
            self.tableWidget_3.insertRow(0)
            flag=0
            for row, form in enumerate(data):
                for column, item in enumerate(form):
                    if flag==0:
                       flag=1
                       continue
                    self.tableWidget_3.setItem(row, column-1, QTableWidgetItem(str(item)))
                    column += 1

                row_position = self.tableWidget_3.rowCount()
                self.tableWidget_3.insertRow(row_position)
                flag=0
            self.tableWidget_3.removeRow(row + 1)

    def Show_favs(self):
        self.db = MySQLdb.connect(host='localhost', user='root', password=pwd, db=dbname)
        self.cur = self.db.cursor()

        self.cur.execute(''' SELECT * FROM favs WHERE user_name=%s''', global_username)
        data = self.cur.fetchall()
        self.tableWidget_3.clearContents()

        if data:
            self.tableWidget_3.setRowCount(0)
            self.tableWidget_3.insertRow(0)
            flag=0
            for row, form in enumerate(data):
                for column, item in enumerate(form):
                    if flag==0:
                       flag=1
                       continue
                    self.tableWidget_3.setItem(row, column-1, QTableWidgetItem(str(item)))
                    column += 1

                row_position = self.tableWidget_3.rowCount()
                self.tableWidget_3.insertRow(row_position)
                flag=0
            self.tableWidget_3.removeRow(row + 1)

        
    ########################################
    #########  UI Themes #################

    def Dark_Blue_Theme(self):
        style = open('themes/darkblue.css', 'r')
        style = style.read()
        self.setStyleSheet(style)

    def Dark_Gray_Theme(self):
        style = open('themes/darkgray.css', 'r')
        style = style.read()
        self.setStyleSheet(style)

    def Dark_Orange_Theme(self):
        style = open('themes/darkorange.css', 'r')
        style = style.read()
        self.setStyleSheet(style)

    def QDark_Theme(self):
        style = open('themes/qdark.css', 'r')
        style = style.read()
        self.setStyleSheet(style)


def main():
    app = QApplication(sys.argv)
    window = Login()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
