import tensorflow as tf
import settings
import models
import numpy as np
import scipy.misc


def loss(sess, model):
    content_layers = settings.CONTENT_LOSS_LAYERS
    sess.run(tf.assign(model.net['input'], model.content))
    content_loss = 0.0
    for layer_name, weight in content_layers:
        p = sess.run(model.net[layer_name])
        x = model.net[layer_name]
        M = p.shape[1] * p.shape[2]
        N = p.shape[3]
        content_loss += (1.0 / (2 * M * N)) * tf.reduce_sum(tf.pow(p - x, 2)) * weight
    content_loss /= len(content_layers)

    style_layers = settings.STYLE_LOSS_LAYERS
    sess.run(tf.assign(model.net['input'], model.style))
    style_loss = 0.0
    for layer_name, weight in style_layers:
        a = sess.run(model.net[layer_name])
        x = model.net[layer_name]
        M = a.shape[1] * a.shape[2]
        N = a.shape[3]
        A = gram(a, M, N)
        G = gram(x, M, N)
        style_loss += (1.0 / (4 * M * M * N * N)) * tf.reduce_sum(tf.pow(G - A, 2)) * weight
    style_loss /= len(style_layers)
    loss = settings.ALPHA * content_loss + settings.BETA * style_loss

    return loss


def gram(x, size, deep):
    x = tf.reshape(x, (size, deep))
    g = tf.matmul(tf.transpose(x), x)
    return g


def train(img1,img2):
    model = models.Model(img1, img2)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        cost = loss(sess, model)
        optimizer = tf.train.AdamOptimizer(1.0).minimize(cost)
        sess.run(tf.global_variables_initializer())
        sess.run(tf.assign(model.net['input'], model.random_img))
        for step in range(settings.TRAIN_STEPS):
            sess.run(optimizer)
            if step % 50 == 0:
                print('step {} is down.'.format(step))
                img = sess.run(model.net['input'])
                img += settings.IMAGE_MEAN_VALUE
                img = img[0]
                img = np.clip(img, 0, 255).astype(np.uint8)
                #保存中间结果
                #scipy.misc.imsave('{}-{}.jpg'.format(settings.OUTPUT_IMAGE,step), img)
        img = sess.run(model.net['input'])
        img += settings.IMAGE_MEAN_VALUE
        img = img[0]
        img = np.clip(img, 0, 255).astype(np.uint8)
        #保存最终结果
        #scipy.misc.imsave('{}.jpg'.format(settings.OUTPUT_IMAGE), img)
        return img
