Art Album——一款基于VGG19的风格转换相册

前期准备：
CPU: Intel(R) Core(TM) i7-7700 CPU @ 3.60GHz GPU：NVIDIA GeForce GTX 2080TI
python3.6
tensorflow-gpu==1.1.0
numpy
PyMySQL==0.9.2

预训练模型下载地址：
https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg19_weights_tf_dim_ordering_tf_kernels.h5

运行步骤：
1.配置所需环境：pip install -r requirements.txt
2.加载导航栏图标:pyrcc5 -o icons_rc.py icons.qrc
3.建立数据库和表格:（1）登录mysql：mysql -h localhost -u username -p password
(2)运行sql文件：source db.sql
4.运行程序：python index.py

引用：
K.Simonyan, A.Zisserman
Very Deep Convolutional Networks for Large-Scale Image Recognition
International Conference on Learning Representations, 2015







